package migrations

import (
	"context"
	"fmt"
	"github.com/spf13/cobra"
	"gitlab.com/witchbrew/go/migrategrpc"
	"google.golang.org/grpc"
)

var RootCmd = &cobra.Command{
	Use:   "migrations",
	Short: "Manage migrations",
}

func init() {
	statusCmd.Flags().StringVar(&host, "host", "localhost:50051", "migrate gRPC server port")
	RootCmd.AddCommand(statusCmd)
}

var host string

var statusCmd = &cobra.Command{
	Use:   "status",
	Short: "Get status of migrations",
	RunE: func(cmd *cobra.Command, args []string) error {
		clientConn, err := grpc.Dial(host, grpc.WithInsecure(), grpc.WithBlock())
		if err != nil {
			return err
		}
		client := migrategrpc.NewMigrateClient(clientConn)
		status, err := client.GetStatus(context.Background(), &migrategrpc.Empty{})
		if err != nil {
			return err
		}
		fmt.Printf("version %d\ndirty %t\n", status.Version, status.Dirty)
		return nil
	},
}
