package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
	"gitlab.com/witchbrew/september/go/september-cli/cmd/migrations"
	"os"
)

var rootCmd = &cobra.Command{
	Use: "september-cli",
}

func init() {
	rootCmd.AddCommand(migrations.RootCmd)
}

func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
