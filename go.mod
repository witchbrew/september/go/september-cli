module gitlab.com/witchbrew/september/go/september-cli

go 1.15

require (
	github.com/spf13/cobra v1.0.0
	gitlab.com/witchbrew/go/migrategrpc v0.0.0-20201001131032-edb94e9d6558
	google.golang.org/grpc v1.32.0
)
