package main

import "gitlab.com/witchbrew/september/go/september-cli/cmd"

func main() {
	cmd.Execute()
}
